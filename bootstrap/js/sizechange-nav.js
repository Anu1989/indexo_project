$(document).ready(function(){
$(window).scroll(function(){
    if($(this).scrollTop()>50){
        $("header").removeClass("large").addClass("small");
        $(".back-to-top").fadeIn();//to scroll up with arrow
    }
    else{
        $("header").removeClass("small").addClass("large");
        $(".back-to-top").fadeOut();//to scroll up with arrow
    }

     $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');//to scroll up with arrow

});
})
